require('isomorphic-fetch');
// const { URL } = require('url');
const querystring = require('querystring');

class TraductionModule {
    params = {
        "sl": "fr",
        "tl": "en",
        "dt": "t"
    };

    async getTraduction(string) {
        this.params["q"] = string;

        // const url = new URL('https://translate.googleapis.com/translate_a/single?client=gtx&');
        // Object.keys(this.params).forEach(key => url.searchParams.append(key, this.params[key]));

        const url = 'https://translate.googleapis.com/translate_a/single?client=gtx&' + querystring.stringify(this.params);

        const response = await fetch(url);
        return response.json();
    }
};

module.exports.TraductionModule = TraductionModule;