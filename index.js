const { TraductionModule } = require('./TraductionModule.js');

(async function main() {
    const traductionModule = new TraductionModule();

    const traductionResult = await traductionModule.getTraduction("Création d'un module pour le cours de NodeJS");

    console.log(`Traduction: "${traductionResult[0][0][1]}" (fr) => "${traductionResult[0][0][0]}" (en)`);
})();